library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PXL_VGA is
    port (
        clk         : in  STD_LOGIC;
        vga_red     : out STD_LOGIC_VECTOR(4 downto 0);
        vga_green   : out STD_LOGIC_VECTOR(5 downto 0);
        vga_blue    : out STD_LOGIC_VECTOR(4 downto 0);
        vga_hsync   : out STD_LOGIC;
        vga_vsync   : out STD_LOGIC;
       -- frame_addr  : out STD_LOGIC_VECTOR(17 downto 0);
       -- frame_pixel : in  STD_LOGIC_VECTOR(11 downto 0);
        btn          : in  STD_LOGIC_VECTOR(6 downto 0);
        sw          : in  STD_LOGIC_VECTOR(3 downto 0)--;
       -- tclk        : in std_logic
    );
end PXL_VGA;

architecture Behavioral of PXL_VGA is
   -- Timing constants
   constant hRez       : natural := 640;
   constant hStartSync : natural := 640+16;
   constant hEndSync   : natural := 640+16+96;
   constant hMaxCount  : natural := 640+16+96+48;

   constant vRez       : natural := 480;
   constant vStartSync : natural := 480+10;
   constant vEndSync   : natural := 480+10+2;
   constant vMaxCount  : natural := 480+10+2+33;
   
   constant hsync_active : std_logic := '0';
   constant vsync_active : std_logic := '0';

   --constant hRez       : natural := 800;
   --constant hStartSync : natural := 800+56;
   --constant hEndSync   : natural := 800+56+120;
   --constant hMaxCount  : natural := 800+56+120+64;

   --constant vRez       : natural := 600;
   --constant vStartSync : natural := 600+37;
   --constant vEndSync   : natural := 600+37+6;
   --constant vMaxCount  : natural := 600+37+6+23;

   --constant hsync_active : std_logic := '1';
   --constant vsync_active : std_logic := '1';

   signal hCounter : unsigned( 9 downto 0) := (others => '0');
   signal vCounter : unsigned( 9 downto 0) := (others => '0');
   signal pCounter : unsigned(9 downto 0) :=(others=>'0');
   signal blank    : std_logic := '1';
   signal clk25    : std_logic;
   signal tclk      : std_logic;
    
   signal h_loc_1 :   integer     :=100;
   signal v_loc_1 :   integer     :=100;
   
   signal h_loc_2 :   integer     :=100;
   signal v_loc_2 :   integer     :=240;
 
   signal h_loc_3 :   integer     :=100;
   signal v_loc_3 :   integer     :=380;
   
   signal h_loc_4 :   integer     :=540;
   signal v_loc_4 :   integer     :=100;
   
   signal h_loc_5 :   integer     :=540;
   signal v_loc_5 :   integer     :=240;
   
   signal h_loc_6 :   integer     :=540;
   signal v_loc_6 :   integer     :=380;
    
   signal h_loc_7 :   integer     :=320;
   signal v_loc_7 :   integer     :=240;    
   
   signal size  :   integer     :=20;
   signal counter : integer     :=0;
   
   signal value: integer :=6;
   
component clk_25_wizard
port
(-- Clock in ports
  clk           : in     std_logic;
  -- Clock out ports
  clk25          : out    std_logic
);
end component;

begin

clk_gen : clk_25_wizard
   port map ( 
   -- Clock in ports
   clk => clk,
  -- Clock out ports  
   clk25 => clk25              
 );

process(clk25)
begin
if rising_edge(clk25) then
    if counter >=25000 then
        tclk <= not tclk;
        counter <=0;
    else
        counter <= counter+1;
     end if;
end if;
end process;

-- 1 => LED 4
-- 2 => LED 1 LED 7
-- 3 => LED 1 LED 7 LED 4
-- 4 => LED 1 LED 3 LED 5 LED 7
-- 5 => LED 1 LED 3 LED 4 LED 5 LED 7
-- 6 => LED 1 LED 2 LED 3 LED 5 LED 6 LED 7

process(btn)
begin
   case (btn) is
      when "0001" => --1
         value <=1;
      when "0010" => --2
         value <=2;
      when "0011" => --3
         value <=3;
      when "0100" => --4
         value <=4;
      when "0101" => --5
         value <=5;
      when "0110" => --6
         value <=6;
      when others =>
         value <=0;
   end case;
 end process;	


--process (tclk)
--begin
--   if rising_edge(tclk) then
--    if btn(0) ='1' then
--       h_loc<=h_loc + 1;
--    elsif btn(1) = '1' then
--       h_loc <= h_loc -1;
--    elsif btn(2) = '1' then
--        v_loc <= v_loc +1;
--    elsif btn(3) = '1' then
--         v_loc <= v_loc -1;
--    end if;
--   end if;
--end process;

process(clk25)
begin
if rising_edge(clk25) then
     
         -- Count the lines and rows
         if hCounter = hMaxCount-1 then
            hCounter <= (others => '0');
            if vCounter = vMaxCount-1 then
               vCounter <= (others => '0');
            else
               vCounter <= vCounter+1;
            end if;
         else
            hCounter <= hCounter+1;
         end if;

         if blank = '0' then
            --if ((hCounter < h_loc - size) and (vCounter < v_loc - size)) or ((hCounter > h_loc + size) and (vCounter > v_loc + size)) or ((hCounter > h_loc - size) and (vCounter > v_loc + size))or ((hCounter > h_loc + size) and (vCounter > v_loc - size))then
            if  ((hCounter < h_loc_1-size or hCounter> h_loc_1 +size or vCounter < v_loc_1-size or vCounter>v_loc_1+size) or btn(0) ='0')
                and ((hCounter < h_loc_2-size or hCounter> h_loc_2 +size or vCounter < v_loc_2-size or vCounter>v_loc_2+size) or btn(1) ='0')
                and ((hCounter < h_loc_3-size or hCounter> h_loc_3 +size or vCounter < v_loc_3-size or vCounter>v_loc_3+size) or btn(2) ='0')
                and ((hCounter < h_loc_4-size or hCounter> h_loc_4 +size or vCounter < v_loc_4-size or vCounter>v_loc_4+size) or btn(3) ='0')
                and ((hCounter < h_loc_5-size or hCounter> h_loc_5 +size or vCounter < v_loc_5-size or vCounter>v_loc_5+size) or btn(4) ='0')
                and ((hCounter < h_loc_6-size or hCounter> h_loc_6 +size or vCounter < v_loc_6-size or vCounter>v_loc_6+size) or btn(5) ='0')
                and ((hCounter < h_loc_7-size or hCounter> h_loc_7 +size or vCounter < v_loc_7-size or vCounter>v_loc_7+size) or btn(6) ='0')
            then
                vga_red     <= sw(0) & "0000";
                vga_green   <= sw(1) & "00000";
                vga_blue    <= sw(2) & sw(3) & "000";
--            elsif hCounter < h_loc_2-size or hCounter> h_loc_2 +size or vCounter < v_loc_2-size or vCounter>v_loc_2+size
--            then
--                vga_red     <= sw(0) & "0000";
--                vga_green   <= sw(1) & "00000";
--                vga_blue    <= sw(2) & sw(3) & "000";
--            elsif (hCounter < h_loc_3-size or hCounter> h_loc_3 +size or vCounter < v_loc_3-size or vCounter>v_loc_3+size) 
--            then
--                vga_red     <= sw(0) & "0000";
--                vga_green   <= sw(1) & "00000";
--                vga_blue    <= sw(2) & sw(3) & "000";
--            elsif (hCounter < h_loc_4-size or hCounter> h_loc_4 +size or vCounter < v_loc_4-size or vCounter>v_loc_4+size)
--            then
--               vga_red     <= sw(0) & "0000";
--               vga_green   <= sw(1) & "00000";
--               vga_blue    <= sw(2) & sw(3) & "000";
--            elsif (hCounter < h_loc_5-size or hCounter> h_loc_5 +size or vCounter < v_loc_5-size or vCounter>v_loc_5+size)
--            then
--               vga_red     <= sw(0) & "0000";
--               vga_green   <= sw(1) & "00000";
--               vga_blue    <= sw(2) & sw(3) & "000";
--            elsif (hCounter < h_loc_6-size or hCounter> h_loc_6 +size or vCounter < v_loc_6-size or vCounter>v_loc_6+size)
--            then
--               vga_red     <= sw(0) & "0000";
--               vga_green   <= sw(1) & "00000";
--               vga_blue    <= sw(2) & sw(3) & "000";
--            elsif (hCounter < h_loc_7-size or hCounter> h_loc_7 +size or vCounter < v_loc_7-size or vCounter>v_loc_7+size)
--            then
--               vga_red     <= sw(0) & "0000";
--               vga_green   <= sw(1) & "00000";
--               vga_blue    <= sw(2) & sw(3) & "000";
            else
                vga_red     <= "11111";
                vga_green   <= "000000";
                vga_blue    <= "00000";
            end if;
         else
            vga_red   <= (others => '0');
            vga_green <= (others => '0');
            vga_blue  <= (others => '0');
         end if;

         if vCounter  >= vRez then
            --address <= (others => '0');
            blank <= '1';
         else
            if hCounter  < 640 then
               blank <= '0';
               --address <= address+1;
            else
               blank <= '1';
            end if;
         end if;

         -- Are we in the hSync pulse? (one has been added to include frame_buffer_latency)
         if hCounter > hStartSync and hCounter <= hEndSync then
            vga_hSync <= hsync_active;
         else
            vga_hSync <= not hsync_active;
         end if;

         -- Are we in the vSync pulse?
         if vCounter >= vStartSync and vCounter < vEndSync then
            vga_vSync <= vsync_active;
         else
            vga_vSync <= not vsync_active;
         end if;
      end if;
   end process;
end Behavioral;
